ast   "Asturian" "Asturianu" 75%
de    "German" "Deutsch" 100%
de_CH "German (Switzerland)" "Deutsch (Schweiz)" 100%
en    "English" "English" 100%
en_AU "English (Australia)" "English (Australia)" 71%
es    "Spanish" "Español" 100%
fr    "French" "Français" 100%
ga    "Irish" "Irish" 30%
it    "Italian" "Italiano" 100%
hu    "Hungarian" "Magyar" 44%
nl    "Dutch" "Nederlands" 58%
pl    "Polish" "Polski" 70%
pt    "Portuguese" "Português" 79%
pt_BR "Portuguese (Brazil)" "Português (Brasil)" 100%
ro    "Romanian" "Romana" 69%
fi    "Finnish" "Suomi" 97%
sv    "Swedish" "Svenska" 97%
tr    "Turkish" "Türkçe" 51%
cs    "Czech" "Čeština" 32%
el    "Greek" "Ελληνική" 45%
be    "Belarusian" "Беларуская" 50%
bg    "Bulgarian" "Български" 59%
ru    "Russian" "Русский" 100%
sr    "Serbian" "Српски" 60%
uk    "Ukrainian" "Українська" 47%
zh_CN "Chinese (China)" "中文" 58%
zh_TW "Chinese (Taiwan)" "國語" 57%
ja_JP "Japanese" "日本語" 100%
ko    "Korean" "한국의" 36%
